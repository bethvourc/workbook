import React from 'react';
import Search from './Search'; // Import the Search component
import './App.css'; // Your main CSS file

const App: React.FC = () => {
  return (
    <div className="app">
      <Search />
    </div>
  );
};

export default App;
