import React from 'react';
import './Search.css'; // Ensure your CSS file matches this import
import googlelogo from './assets/gooogle.png'; // Adjust the import if necessary

const Search: React.FC = () => {
  return (
    <div className="search-container">
      <div className="logo-container">
        {/* Added 'logo' class for styling */}
        <img src={googlelogo} alt='Google Logo' className='logo'/>
      </div>
      <input type="text" className="search-input" aria-label="Search" />
      <div className="buttons-container">
        <button className="search-button">Google Search</button>
        <button className="lucky-button">I'm Feeling Lucky</button>
      </div>
    </div>
  );
};

export default Search;
